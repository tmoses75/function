/*
function/list
functions.go
a functional package for Go applications
by Todd Moses
*/

package list

import (
	"math/rand"
	"sort"
	"sync"
	"time"
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

/*
Map iterates over a list of elements, yielding each in turn to an iteratee function.
iter Iteratee

returns *List
*/
func (lst *List) Map(iter Iteratee) *List {

	if len(lst.Data) == 0 || iter == nil {
		return lst
	}

	// if we are dealing with 100 or more items
	// do this concurrently
	if len(lst.Data) > 100 {
		// we will divide into 100
		itemsPerWorker := 50
		ct := float64(len(lst.Data) / itemsPerWorker)
		numWorkers := round(ct, .5, 0)
		return eachConcurrent(lst, iter, int(numWorkers), itemsPerWorker)
	}

	return each(lst, iter)
}

func each(lst *List, iter Iteratee) *List {

	newList := new(List)

	for _, v := range lst.Data {
		newV := iter(v)
		newList.Data = append(newList.Data, newV)
	}

	return newList
}

func eachConcurrent(lst *List, iter Iteratee, numWorkers int, itemsPerWorker int) *List {

	newList := new(List)

	var wg sync.WaitGroup
	wg.Add(numWorkers)

	startIndex := 0
	endIndex := itemsPerWorker - 1

	if endIndex > (len(lst.Data) - 1) {
		return newList
	}

	// divide work for each worker
	for i := 0; i < numWorkers; i++ {

		go func(i int) {
			defer wg.Done()

			if i > 0 {
				startIndex = (endIndex + 1)
				endIndex = startIndex + (itemsPerWorker - 1)
				if endIndex > (len(lst.Data) - 1) {
					endIndex = (len(lst.Data) - 1)
				}
			}

			for _, iv := range lst.Data[startIndex:endIndex] {
				newV := iter(iv)
				newList.Data = append(newList.Data, newV)
			}
		}(i)

	}

	wg.Wait()

	return newList
}

/*
Reduce reduce boils down a list of values into a single value.
Memo is the initial state of the reduction, and each successive
step of it should be returned by iteratee. if 0 then the first
iteration is ignored.
iter IterateeRed
memo float64

returns float64
*/
func (lst *List) Reduce(iter IterateeReduce, memo float64) float64 {

	if len(lst.Data) == 0 || iter == nil {
		return 0
	}

	return reduce(lst, iter, memo)
}

func reduce(lst *List, iter IterateeReduce, memo float64) float64 {

	total := float64(0)

	for _, v := range lst.Data {
		if memo != 0 {
			total = iter(v, memo)
		}
		memo = v
	}

	return total
}

/*
SortBy returns a sorted copy of list, ranked in ascending order by the results of
running each value through iteratee.
iter Iteratee

returns *List
*/
func (lst *List) SortBy(iter Iteratee) *List {

	if len(lst.Data) == 0 || iter == nil {
		return lst
	}

	newList := new(List)

	// run each to perform iteratee
	newList = lst.Map(iter)

	// sort asc
	sort.Float64s(newList.Data)

	return newList
}

/*
Filter looks through each value in the List, returning a List of all the values that pass a truth test.
pred Predicate

returns *List
*/
func (lst *List) Filter(pred Predicate) *List {

	if len(lst.Data) == 0 || pred == nil {
		return lst
	}

	// if we are dealing with 100 or more items
	// do this concurrently
	if len(lst.Data) > 100 {
		// we will divide into 100
		itemsPerWorker := 50
		ct := float64(len(lst.Data) / itemsPerWorker)
		numWorkers := round(ct, .5, 0)
		return truthTestConcurrent(lst, pred, true, int(numWorkers), itemsPerWorker)
	}

	return truthTest(lst, pred, true)
}

func truthTest(lst *List, pred Predicate, isTrue bool) *List {

	newList := new(List)

	for _, v := range lst.Data {
		if pred(v) == isTrue {
			newList.Data = append(newList.Data, v)
		}

	}

	return newList
}

func truthTestConcurrent(lst *List, pred Predicate, isTrue bool, numWorkers int, itemsPerWorker int) *List {

	newList := new(List)

	var wg sync.WaitGroup
	wg.Add(numWorkers)

	startIndex := 0
	endIndex := itemsPerWorker - 1

	if endIndex > (len(lst.Data) - 1) {
		return newList
	}

	// divide work for each worker
	for i := 0; i < numWorkers; i++ {

		go func(i int) {
			defer wg.Done()

			if i > 0 {
				startIndex = (endIndex + 1)
				endIndex = startIndex + (itemsPerWorker - 1)
				if endIndex > (len(lst.Data) - 1) {
					endIndex = (len(lst.Data) - 1)
				}
			}

			for _, iv := range lst.Data[startIndex:endIndex] {
				if pred(iv) == isTrue {
					newList.Data = append(newList.Data, iv)
				}
			}
		}(i)

	}

	wg.Wait()

	return newList
}

/*
Reject returns the values in List without the elements that the truth test (predicate) passes.
The opposite of filter.
pred Predicate

returns *List
*/
func (lst *List) Reject(pred Predicate) *List {

	if len(lst.Data) == 0 || pred == nil {
		return lst
	}

	// if we are dealing with 100 or more items
	// do this concurrently
	if len(lst.Data) > 100 {
		// we will divide into 100
		itemsPerWorker := 50
		ct := float64(len(lst.Data) / itemsPerWorker)
		numWorkers := round(ct, .5, 0)
		return truthTestConcurrent(lst, pred, false, int(numWorkers), itemsPerWorker)
	}

	return truthTest(lst, pred, false)
}

/*
Every returns true if all of the values in the List pass the predicate truth test.
Short-circuits and stops traversing the list if a false element is found.
pred Predicate

returns bool
*/
func (lst *List) Every(pred Predicate) bool {

	if len(lst.Data) == 0 || pred == nil {
		return false
	}

	// if we are dealing with 100 or more items
	// do this concurrently
	if len(lst.Data) > 100 {
		// we will divide into 100
		itemsPerWorker := 50
		ct := float64(len(lst.Data) / itemsPerWorker)
		numWorkers := round(ct, .5, 0)
		return hasTrueConcurrent(lst, pred, true, int(numWorkers), itemsPerWorker)
	}

	return hasTrue(lst, pred, true)
}

func hasTrue(lst *List, pred Predicate, isTrue bool) bool {

	for _, v := range lst.Data {
		if pred(v) == !isTrue {
			return false
		}
	}

	return true
}

func hasTrueConcurrent(lst *List, pred Predicate, isTrue bool, numWorkers int, itemsPerWorker int) bool {

	messages := make(chan bool)

	var wg sync.WaitGroup
	wg.Add(numWorkers)

	startIndex := 0
	endIndex := itemsPerWorker - 1

	if endIndex > (len(lst.Data) - 1) {
		return false
	}

	// divide work for each worker
	for i := 0; i < numWorkers; i++ {

		go func(i int) {
			defer wg.Done()

			if i > 0 {
				startIndex = (endIndex + 1)
				endIndex = startIndex + (itemsPerWorker - 1)
				if endIndex > (len(lst.Data) - 1) {
					endIndex = (len(lst.Data) - 1)
				}
			}

			for _, iv := range lst.Data[startIndex:endIndex] {
				if pred(iv) == isTrue {
					messages <- true
				}
			}
		}(i)

	}

	result := true

	go func() {
		for i := range messages {
			if i == !isTrue {
				result = false
				wg.Done()
			}
		}
	}()

	wg.Wait()

	return result
}

/*
Some returns true if any of the values in the List pass the predicate truth test.
Short-circuits and stops traversing the list if a true element is found.
pred Predicate

returns bool
*/
func (lst *List) Some(pred Predicate) bool {

	if len(lst.Data) == 0 || pred == nil {
		return false
	}

	// if we are dealing with 100 or more items
	// do this concurrently
	if len(lst.Data) > 100 {
		// we will divide into 100
		itemsPerWorker := 50
		ct := float64(len(lst.Data) / itemsPerWorker)
		numWorkers := round(ct, .5, 0)
		return hasTrueConcurrent(lst, pred, false, int(numWorkers), itemsPerWorker)
	}

	return hasTrue(lst, pred, false)
}

/*
Contains returns true if the value is present in the list.
val float64

returns bool
*/
func (lst *List) Contains(val float64) bool {

	if len(lst.Data) == 0 {
		return false
	}

	// create a predicate truth function
	// and use Some method to process
	pred := func(v float64) bool {
		if v == val {
			return true
		}
		return false
	}

	return lst.Some(pred)
}

/*
Unique removes duplicate values from a list

returns *List
*/
func (lst *List) Unique() *List {

	if len(lst.Data) > 100 {
		// we will divide into 100
		itemsPerWorker := 50
		ct := float64(len(lst.Data) / itemsPerWorker)
		numWorkers := round(ct, .5, 0)
		return uniqueConcurrent(lst, int(numWorkers), itemsPerWorker)
	}

	return unique(lst)
}

func unique(lst *List) *List {

	newList := new(List)
	// Use map to record duplicates as we find them.
	encountered := map[float64]bool{}

	for v := range lst.Data {
		if encountered[lst.Data[v]] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[lst.Data[v]] = true
			// Append to result slice.
			newList.Data = append(newList.Data, lst.Data[v])
		}
	}

	return newList
}

func uniqueConcurrent(lst *List, numWorkers int, itemsPerWorker int) *List {

	newList := new(List)

	messages := make(chan float64)

	var wg sync.WaitGroup
	wg.Add(numWorkers)

	startIndex := 0
	endIndex := itemsPerWorker - 1

	if endIndex > (len(lst.Data) - 1) {
		return lst
	}

	// divide work for each worker
	for i := 0; i < numWorkers; i++ {

		go func(i int) {
			defer wg.Done()

			if i > 0 {
				startIndex = (endIndex + 1)
				endIndex = startIndex + (itemsPerWorker - 1)
				if endIndex > (len(lst.Data) - 1) {
					endIndex = (len(lst.Data) - 1)
				}
			}

			for iv := startIndex; iv <= endIndex; iv++ {

				// Scan slice for a previous element of the same value.
				exists := false
				for v := 0; v < i; v++ {
					if lst.Data[v] == lst.Data[iv] {
						exists = true
						break
					}
				}
				// If no previous element exists, append this one.
				if !exists {
					messages <- lst.Data[iv]
				}
			}
		}(i)

	}

	go func() {
		for i := range messages {
			newList.Data = append(newList.Data, i)
		}
	}()

	wg.Wait()

	return newList
}

/*
Union computes the union of the passed-in list and calling list:
the list of unique items, optional in order, that are present in one or more of the lists.
doSort bool
cmpLists ...List

returns *List
*/
func (lst *List) Union(doSort bool, cmpLists ...*List) *List {

	if len(lst.Data) == 0 || len(cmpLists) == 0 {
		return lst
	}

	newList := new(List)
	newCmpList := new(List)

	// put all passed items into single list
	for _, v := range cmpLists {
		newCmpList.Data = append(newCmpList.Data, v.Data...)
	}

	// get union of passed list and original
	newList = intersect(lst, newCmpList)

	// make list unique
	newList = newList.Unique()

	// sort
	if doSort {
		sort.Float64s(newList.Data)
	}

	return newList
}

/*
Intersection computes the list of values that are the intersection of all the lists.
Each value in the result is present in each of the lists.
doSort bool
cmpLists ..List

returns *List
*/
func (lst *List) Intersection(doSort bool, cmpLists ...*List) *List {

	if len(lst.Data) == 0 || len(cmpLists) == 0 {
		return lst
	}

	newList := new(List)

	if len(cmpLists) > 1 {
		newList = intersectMult(lst, cmpLists)
	} else {
		newList = intersect(lst, cmpLists[0])
	}

	// make list unique
	newList = newList.Unique()

	//  sort
	if doSort {
		sort.Float64s(newList.Data)
	}

	return newList
}

func intersect(lst *List, cmpList *List) *List {

	// create a predicate to filter by
	pred := func(v float64) bool {
		if cmpList.Contains(v) {
			return true
		}
		return false
	}

	return lst.Filter(pred)
}

func intersectMult(lst *List, cmpLists []*List) *List {

	newList := new(List)

	for _, cList := range cmpLists {
		// do not check list against itself
		if cList != cmpLists[0] {
			// get intersection of list with all other lists
			lstNew := intersect(cList, cmpLists[0])
			// now get intersection of new list and main list
			lstNewMn := intersect(lst, lstNew)
			newList.Data = append(newList.Data, lstNewMn.Data...)

		}
	}

	return newList
}

/*
Difference returns the values from list that are not present in the other list.
doSort bool
cmpLists ...*List

returns *List
*/
func (lst *List) Difference(doSort bool, cmpLists ...*List) *List {

	if len(lst.Data) == 0 || len(cmpLists) == 0 {
		return lst
	}

	newList := new(List)

	if len(cmpLists) > 1 {
		newList = diffMult(lst, cmpLists)
	} else {
		newList = diff(lst, cmpLists[0])
	}

	// make list unique
	newList = newList.Unique()

	//  sort
	if doSort {
		sort.Float64s(newList.Data)
	}

	return newList
}

func diff(lst *List, cmpList *List) *List {

	// create a predicate to filter by
	pred := func(v float64) bool {
		if cmpList.Contains(v) == false {
			return true
		}
		return false
	}

	return lst.Filter(pred)
}

func diffMult(lst *List, cmpLists []*List) *List {

	newList := new(List)

	for _, cList := range cmpLists {
		// do not check list against itself
		if cList != cmpLists[0] {
			// get intersection of list with all other lists
			lstNew := diff(cList, cmpLists[0])
			// now get intersection of new list and main list
			lstNewMn := diff(lst, lstNew)
			newList.Data = append(newList.Data, lstNewMn.Data...)

		}
	}

	return newList
}

/*
Shuffle eturns a shuffled copy of the list, using a version of
the Fisher-Yates algorithm.

returns *List
*/
func (lst *List) Shuffle() *List {

	if len(lst.Data) == 0 {
		return lst
	}

	newList := new(List)
	newList.Data = lst.Data

	n := len(lst.Data)
	for i := n - 1; i > 0; i-- {
		j := rand.Intn(i + 1)
		newList.Data[i], newList.Data[j] = newList.Data[j], newList.Data[i]
	}

	return newList
}

/*
Sample produces a random sample from the list. Pass a number to return n random
elements from the list. Otherwise a single random item will be returned.
n int

returns *List
*/
func (lst *List) Sample(n int) *List {

	if len(lst.Data) == 0 {
		return lst
	}

	newList := new(List)

	// if no n is passed then return a single random number from the list
	if n <= 0 {
		ind := rand.Intn(len(lst.Data) - 1)
		newList.Data = append(newList.Data, lst.Data[ind])
		return newList
	}

	if n > len(lst.Data) {
		n = len(lst.Data)
	}

	for i := 0; i < n; i++ {
		j := rand.Intn(i + 1)
		newList.Data = append(newList.Data, lst.Data[j])
	}

	return newList
}

/*
Head returns the first element of a list.
Passing n will return the first n elements of the list.
n int

returns *List
*/
func (lst *List) Head(n int) *List {

	if len(lst.Data) == 0 {
		return lst
	}

	newList := new(List)

	if n <= 0 {
		newList.Data = append(newList.Data, lst.Data[0])
		return newList
	}

	if n > len(lst.Data) {
		n = len(lst.Data)
	}

	for i := 0; i < n; i++ {
		newList.Data = append(newList.Data, lst.Data[i])
	}

	return newList
}

/*
Tail returns the last element of a list.
Passing n will return the last n elements of the list.
n int

returns *List
*/
func (lst *List) Tail(n int) *List {

	if len(lst.Data) == 0 {
		return lst
	}

	newList := new(List)

	if n <= 0 {
		newList.Data = append(newList.Data, lst.Data[len(lst.Data)-1])
		return newList
	}

	if n > len(lst.Data) {
		n = len(lst.Data)
	}

	for i := 0; i < n; i++ {
		ind := (len(lst.Data) - 1) - i
		if ind < 0 {
			ind = 0
		}
		newList.Data = append(newList.Data, lst.Data[ind])
	}

	return newList
}

/*
IndexOf returns the index at which value can be found in the
list, or -1 if value is not present.
value float64

returns in
*/
func (lst *List) IndexOf(value float64) int {

	ind := -1

	if len(lst.Data) == 0 || value == 0 {
		return ind
	}

	for i := 0; i < len(lst.Data); i++ {
		if lst.Data[i] == value {
			return i
		}
	}

	return ind
}
