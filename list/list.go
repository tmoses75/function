/*
function/list
list.go
a functional package for Go applications
by Todd Moses
*/

package list

import (
	"math"
	"sync"
)

/*
List is an object that represents a slice of float64
Data float64 is the values
*/
type List struct {
	Data []float64
}

// Iteratee is a function to manuipulate float64
type Iteratee func(float64) float64

// IterateeReduce is a function to reduce
type IterateeReduce func(float64, float64) float64

// Predicate is a function to returns a truth value
type Predicate func(float64) bool

/*
New generates a list from a slice
slice []float64
returns List
*/
func New(slice []float64) *List {

	lst := new(List)
	lst.Data = slice

	return lst
}

/*
Extract return the Data of the List as a slice
returns []float64
*/
func (lst *List) Extract() []float64 {
	return lst.Data
}

/*
Round rounds a number on a specified location to a specified number of places
val float64 - the value to round
roundOn float64 - the place to round (EX: 1, .5)
places int - the place to round (EXL 0, 1, 2)
'
Example: Round(123.555555, .5, 0) returns 124
'
returns float64
*/
func round(val float64, roundOn float64, places int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit)
	if div >= roundOn {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}
	newVal = round / pow
	return
}

func copyList(lst *List) *List {

	newList := new(List)

	// do not use concurent if small
	if len(lst.Data) < 100 {
		for _, v := range lst.Data {
			newList.Data = append(newList.Data, v)
		}
		return newList
	}

	itemsPerWorker := 50
	ct := float64(len(lst.Data) / itemsPerWorker)
	numWorkers := round(ct, .5, 0)

	var wg sync.WaitGroup
	wg.Add(int(numWorkers))

	startIndex := 0
	endIndex := itemsPerWorker - 1

	if endIndex > (len(lst.Data) - 1) {
		return newList
	}

	// divide work for each worker
	for i := 0; i < int(numWorkers); i++ {

		go func(i int) {
			defer wg.Done()

			if i > 0 {
				startIndex = (endIndex + 1)
				endIndex = startIndex + (itemsPerWorker - 1)
				if endIndex > (len(lst.Data) - 1) {
					endIndex = (len(lst.Data) - 1)
				}
			}

			for _, iv := range lst.Data[startIndex:endIndex] {
				newList.Data = append(newList.Data, iv)
			}
		}(i)

	}

	wg.Wait()

	return newList
}
