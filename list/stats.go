/*
function/list
stats.go
a functional package for Go applications
by Todd Moses
*/

package list

import (
	"math"
	"sort"
	"sync"
)

type sortedMap struct {
	m map[float64]int
	s []float64
}

/*
Sum adds all items in a list together
returns float64
*/
func (lst *List) Sum() float64 {

	if len(lst.Data) == 0 {
		return 0
	}

	if len(lst.Data) > 100 {
		// we will divide into 100
		itemsPerWorker := 50
		ct := float64(len(lst.Data) / itemsPerWorker)
		numWorkers := round(ct, .5, 0)
		return sumConcurrent(lst, int(numWorkers), itemsPerWorker)
	}

	return sum(lst)
}

func sum(lst *List) float64 {
	total := float64(0)
	for _, v := range lst.Data {
		if v != math.NaN() {
			total = total + v
		}
	}
	return total
}

func sumConcurrent(lst *List, numWorkers int, itemsPerWorker int) float64 {
	total := float64(0)

	var wg sync.WaitGroup
	wg.Add(numWorkers)

	startIndex := 0
	endIndex := itemsPerWorker - 1

	if endIndex > (len(lst.Data) - 1) {
		return total
	}

	// divide work for each worker
	for i := 0; i < numWorkers; i++ {

		go func(i int) {
			defer wg.Done()

			for _, v := range lst.Data[startIndex:endIndex] {
				if v != math.NaN() {
					total = total + v
				}
			}

		}(i)

	}

	wg.Wait()

	return total
}

/*
Product multiplies all items in a list together
returns float64
*/
func (lst *List) Product() float64 {
	if len(lst.Data) == 0 {
		return 0
	}

	if len(lst.Data) > 100 {
		// we will divide into 100
		itemsPerWorker := 50
		ct := float64(len(lst.Data) / itemsPerWorker)
		numWorkers := round(ct, .5, 0)
		return prodConcurrent(lst, int(numWorkers), itemsPerWorker)
	}

	return prod(lst)
}

func prod(lst *List) float64 {
	total := float64(0)
	for _, v := range lst.Data {
		if v != math.NaN() {
			total = total * v
		}
	}
	return total
}

func prodConcurrent(lst *List, numWorkers int, itemsPerWorker int) float64 {
	total := float64(0)

	var wg sync.WaitGroup
	wg.Add(numWorkers)

	startIndex := 0
	endIndex := itemsPerWorker - 1

	if endIndex > (len(lst.Data) - 1) {
		return total
	}

	// divide work for each worker
	for i := 0; i < numWorkers; i++ {

		go func(i int) {
			defer wg.Done()

			for _, v := range lst.Data[startIndex:endIndex] {
				if v != math.NaN() {
					total = total * v
				}
			}

		}(i)

	}

	wg.Wait()

	return total
}

/*
Max returns the maximum value in list. If list is empty, returns 0
returns float64
*/
func (lst *List) Max() float64 {

	if len(lst.Data) == 0 {
		return 0
	}

	max, _ := maxMin(lst)

	return max
}

/*
Min returns the minimum value in list. If list is empty, returns 0
returns float64
*/
func (lst *List) Min() float64 {

	if len(lst.Data) == 0 {
		return 0
	}

	_, min := maxMin(lst)

	return min
}

// returns max or min from list
func maxMin(lst *List) (max float64, min float64) {

	tempData := lst.Data

	// sort list
	sort.Float64s(tempData)

	min = tempData[0]
	max = tempData[len(tempData)-1]

	if min == math.NaN() {
		min = 0
	}

	if max == math.NaN() {
		max = 0
	}

	return
}

/*
Mean returns the Average of a list
returns float64
*/
func (lst *List) Mean() float64 {

	if len(lst.Data) == 0 {
		return 0
	}

	numItems := float64(len(lst.Data))
	sum := lst.Sum()

	if sum == 0 {
		return 0
	}

	return sum / numItems
}

/*
Median returns the Median (the middle value) of a list
returns float64
*/
func (lst *List) Median() float64 {

	if len(lst.Data) == 0 {
		return 0
	}

	// Put all the numbers in numerical order.
	numItems := float64(len(lst.Data))
	middleIndex := (numItems / 2)

	// If there is an odd number of results, the median is the middle number.
	if math.Mod(numItems, 2) > 0 {

		middleIndex = round(middleIndex, .5, 0)
		middleIndex = middleIndex - 1

		return lst.Data[int(middleIndex)]
	}

	// If there is an even number of results, the median will be the mean of the two central numbers.
	// Divide List in 2 parts - get last of first part and first and second part
	middleIndex = middleIndex - 1
	centralA := lst.Data[int(middleIndex-1)]
	centralB := lst.Data[int(middleIndex)]

	if centralA == math.NaN() || centralB == math.NaN() {
		return 0
	}

	tot := centralA + centralB

	return (tot / 2)
}

/*
Mode returns the Mode (the most frequently occurring value) of a list
returns float64
*/
func (lst *List) Mode() float64 {

	if len(lst.Data) == 0 {
		return 0
	}

	return mode(lst)
}

func mode(lst *List) float64 {
	freq := map[float64]int{}

	// step 1: get frequency of all values
	// in list
	for _, v := range lst.Data {
		freq[v] = freq[v] + 1
	}

	// step 2: sort by frequency
	keys := sortedKeys(freq)

	// step 3: return largest
	return keys[0]
}

/*
StandardDeviation returns the standard deviation of a supplied set of values (which represent a sample of a population)
returns float64
*/
func (lst *List) StandardDeviation() float64 {

	if len(lst.Data) == 0 {
		return 0
	}

	if len(lst.Data) > 100 {
		return stdDevConcurrent(lst)
	}

	return stdDev(lst)
}

func stdDev(lst *List) float64 {
	newList := new(List)

	med := lst.Median()

	for _, value := range lst.Data {
		newList.Data = append(newList.Data, math.Abs(value-med))
	}

	return newList.Median()
}

func stdDevConcurrent(lst *List) float64 {

	newList := new(List)

	itemsPerWorker := 50
	ct := float64(len(lst.Data) / itemsPerWorker)
	numWorkers := round(ct, .5, 0)

	var wg sync.WaitGroup
	wg.Add(int(numWorkers))

	startIndex := 0
	endIndex := itemsPerWorker - 1

	if endIndex > (len(lst.Data) - 1) {
		return 0
	}

	med := lst.Median()

	// divide work for each worker
	for i := 0; i < int(numWorkers); i++ {

		go func(i int) {
			defer wg.Done()

			if i > 0 {
				startIndex = (endIndex + 1)
				endIndex = startIndex + (itemsPerWorker - 1)
				if endIndex > (len(lst.Data) - 1) {
					endIndex = (len(lst.Data) - 1)
				}
			}

			for _, iv := range lst.Data[startIndex:endIndex] {
				newList.Data = append(newList.Data, math.Abs(iv-med))
			}
		}(i)

	}

	wg.Wait()
	return newList.Median()
}

/*
Variance returns the variance of a supplied set of values (which represent a sample of a population)
returns float64
*/
func (lst *List) Variance() float64 {

	variance := float64(0)

	if len(lst.Data) == 0 {
		return variance
	}

	// Sum the square of the mean subtracted from each number
	mean := lst.Mean()

	for _, n := range lst.Data {
		variance += (n - mean) * (n - mean)
	}

	return variance / float64(len(lst.Data))
}

/*
Rank returns the statistical rank of a given value, within a list
n float64

returns float64
*/
func (lst *List) Rank(n float64) float64 {

	if len(lst.Data) == 0 {
		return -1
	}

	// assign values to a new list
	newList := copyList(lst)

	// sort in asc order
	sort.Float64s(newList.Data)

	for i, v := range newList.Data {
		if v == n {
			return float64((i + 1) / len(newList.Data))
		}
	}

	return 0
}

func (sm *sortedMap) Len() int {
	return len(sm.m)
}

func (sm *sortedMap) Less(i, j int) bool {
	return sm.m[sm.s[i]] > sm.m[sm.s[j]]
}

func (sm *sortedMap) Swap(i, j int) {
	sm.s[i], sm.s[j] = sm.s[j], sm.s[i]
}

func sortedKeys(m map[float64]int) []float64 {
	sm := new(sortedMap)
	sm.m = m
	sm.s = make([]float64, len(m))
	i := 0
	for key, _ := range m {
		sm.s[i] = key
		i++
	}
	sort.Sort(sm)
	return sm.s
}
