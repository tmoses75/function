/*
function/list
functions_test.go
a functional package for Go applications
by Todd Moses
*/

package list

import (
	"fmt"
	"testing"
)

func TestMap(t *testing.T) {

	slice := []float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}

	iter := func(x float64) float64 { return float64(x + 2) }

	lst := New(slice)
	// (lst *List) Each(iter Iteratee) *List
	lst2 := lst.Map(iter)

	fmt.Println(lst2)
}
