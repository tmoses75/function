# Function

This is a Go Package designed to provide functional methods. When needed due to List size, concurrency is used to run calculations. For this reason, it is important to make appropriate settings in the main of your application. For < Go 1.5, You must set the GOMAXPROCS shell environment variable or use the similarly-named function of the runtime package to allow the run-time support to utilize more than one OS thread.

Included are multiple packages to deal with functional, array, and statistical methods. For the most part they are chainable. So that they can be combined to produce results as one would do in a functional language.

## List

The List package is a collection of methods that are ran on a List type. The List type holds a slice of float64. You create one as follows:

```go
slice := []float64{33,44,55,66}
myList := New(slice)
```
To remove your slice from a List, call:

```go
slice := myList.Extract()
```

One you have a List type, the following methods are available:

### Map
Map iterates over a list of elements, yielding each in turn to an iteratee function.

```go
// (lst *List) Map(iter Iteratee) *List
iter := func(x float64) float64 { return float64(x + 2) }
newList := myList.Map(iter)
```

### Reduce
Reduce reduce boils down a list of values into a single value.
Memo is the initial state of the reduction, and each successive
step of it should be returned by iteratee. if 0 then the first
iteration is ignored.

```go
// (lst *List) Reduce(iter IterateeReduce, memo float64) float64
iterRed := func(x float64, memo float64) float64 { return float64(x + memo)}
newList := myList.Map(iter).Reduce(iterRed, 0)
```

### SortBy
SortBy returns a sorted copy of list, ranked in ascending order by the results of
running each value through iteratee.

```go
// (lst *List) SortBy(iter Iteratee) *List 
iter := func(x float64) float64 { return float64(x + 2) }
newList := myList.SortBy(iter)
```

### Filter
Filter looks through each value in the List, returning a List of all the values that pass a truth test.
pred Predicate

```go
// (lst *List) Filter(pred Predicate) *List
pred := func(x float64) bool { if math.Mod(x) == 0 {return true} return false}
newList := myList.Filter(pred)
```

### Reject
Reject returns the values in List without the elements that the truth test (predicate) passes.
The opposite of filter.

```go
// (lst *List) Reject(pred Predicate) *List
pred := func(x float64) bool { if math.Mod(x) == 0 {return true} return false}
newList := myList.Reject(pred)
```

### Every
Every returns true if all of the values in the List pass the predicate truth test.
Short-circuits and stops traversing the list if a false element is found.

```go
// (lst *List) Every(pred Predicate) bool 
pred := func(x float64) bool { if math.Mod(x) == 0 {return true} return false}
newList := myList.Every(pred)
```

### Some
Some returns true if any of the values in the List pass the predicate truth test.
Short-circuits and stops traversing the list if a true element is found.

```go
// (lst *List) Some(pred Predicate) bool
pred := func(x float64) bool { if math.Mod(x) == 0 {return true} return false}
newList := myList.Some(pred)
```

### Contains
Contains returns true if the value is present in the list.

```go
// (lst *List) Contains(val float64) bool
doesContain := myList.Contains(234)
```

### Unique
Unique removes duplicate values from a list

```go
// (lst *List) Unique() *List
newList := myList.Unique()
```

### Union
Union computes the union of the passed-in list and calling list:
the list of unique items, optional in order, that are present in one or more of the lists.

```go
// (lst *List) Union(doSort bool, cmpLists ...*List) *List
newList := myList.Union(false, list1, list2, list3)
```

### Intersection
Intersection computes the list of values that are the intersection of all the lists.
Each value in the result is present in each of the lists.

```go
// (lst *List) Intersection(doSort bool, cmpLists ...*List) *List
newList := myList.Intersection(false, list1, list2, list3)
```

### Difference
Difference returns the values from list that are not present in the other list.

```go
// (lst *List) Difference(doSort bool, cmpLists ...*List) *List
newList := myList.Difference(false, list1, list2, list3)
```

### Shuffle
Shuffle eturns a shuffled copy of the list, using a version of
the Fisher-Yates algorithm.

```go
// (lst *List) Shuffle() *List
newList := myList.Shuffle()
```

### Sample
Sample produces a random sample from the list. Pass a number to return n random
elements from the list. Otherwise a single random item will be returned.

```go
// (lst *List) Sample(n int) *List
newList := myList.Sample(7)
```

### Head
Head returns the first element of a list.Passing n will return the first n elements of the list.

```go
// (lst *List) Head(n int) *List
newList := myList.Head(3)
```

### Tail
Tail returns the last element of a list. Passing n will return the last n elements of the list

```go
// (lst *List) Tail(n int) *List 
newList := myList.Tail(4)
```

### IndexOf
IndexOf returns the index at which value can be found in the list, or -1 if value is not present.

```go
// (lst *List) IndexOf(value float64) int
index := myList.IndexOf(2344)
```

### Sum
Sum adds all items in a list together

```go
// (lst *List) Sum() float64
number := myList.Sum()
```

### Product
Product multiplies all items in a list together

```go
// (lst *List) Product() float64
number := myList.Product()
```

### Max
Max returns the maximum value in list. If list is empty, returns 0.

```go
// (lst *List) Max() float64
max := myList.Max()
```

### Min
Min returns the minimum value in list. If list is empty, returns 0.

```go
// (lst *List) Min() float64
min := myList.Min()
```

### Mean
Mean returns the Average of a list.

```go
// (lst *List) Mean() float64
mean := myList.Mean()
```

### Median
Median returns the Median (the middle value) of a list.

```go
// (lst *List) Median() float64
median := myList.Median()
```

### Mode
Mode returns the Mode (the most frequently occurring value) of a list.

```go
// (lst *List) Mode() float64
mode := myList.Mode
```

### StandardDeviation
StandardDeviation returns the standard deviation of a supplied set of values (which represent a sample of a population)

```go
// (lst *List) StandardDeviation() float64
stdDev := myList.StandardDeviation()

// for the Standard Deviation of a Sample
stdDev := myList.Sample(25).StandardDeviation()
```

### Variance
Variance returns the variance of a supplied set of values (which represent a sample of a population)

```go
// (lst *List) Variance() float64 
variance := myList.Variance()
```

### Rank
Rank returns the statistical rank of a given value, within a list

```go
// (lst *List) Rank(n float64) float64
rank := myList.Rank(443)
```